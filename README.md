# IMDB Training Database
Portion of IMDB database (International Movie Database) for training and performance testing purposes.

## Database Dumps & Formats
- [**Microsoft SQL Server** - `*.BAK`](https://bitbucket.org/tomflidr/imdb-training-database/raw/0650641dd6f6c6f805ad89388530750f3eaa7bb2/Microsoft%20SQL/imdb-2017-06-msbak.7z) (created with MS SQL Server 2014)
- [**Microsoft SQL Server** - `*.SQL`](https://bitbucket.org/tomflidr/imdb-training-database/raw/f883beee518cabc9389f38b8e36557b5d9aa8176/Microsoft%20SQL/imdb-2017-06-mssql.7z)
- [**MySQL Server** - `*.SQL`](https://bitbucket.org/tomflidr/imdb-training-database/raw/7eeaef1491e1ad7f839bd2788de9a3955490fb00/MySQL/imdb-2017-06-mysql.7z)

## Used Sources
Database dumps are created from public IMDB exports in June 2017, located in: 
- ftp://ftp.fu-berlin.de/pub/misc/movies/database/
- ftp://ftp.funet.fi/pub/mirrors/ftp.imdb.com/pub/

Imported tables has been compared for joining by movie title and identified by new integer ids and other necessary ids.

## Scheme
![Printscreen](https://bytebucket.org/tomflidr/imdb-training-database/raw/df9b8377dfb82bac31f17326738a68c4227dd95d/database-diagram.png?token=9c66e0e63b48ca6928805d206860c99bbce7d34e)

## How To Restore
#### Microsoft SQL Server
- **Binary Backup**
    - extract archive `./Microsoft SQL/imdb-2017-06-msbak.7z`
    - restore database from unpacked file `imdb-2017-06-ms-sql-server-2014.bak` through standard way
    - this restoration process usualy takes 1.5 minute
- **SQL Backup**
    - extract archive `./Microsoft SQL/imdb-2017-06-mssql.7z`
    - restore database by unpacked `./imdb-2017-06-mssql/import.bat`
    - configure server connection at the beginning of the file `import.bat`
    - this restoration process usualy takes 6 to 9 hours, depends on your system

#### MySQL
- **SQL Backup**
    - extract archive `./MySQL/imdb-2017-06-mysql.7z`
    - restore database by unpacked `./imdb-2017-06-mysql/import.bat`
      - configure server connection at the beginning of the file `import.bat`
    - this restoration process usualy takes 3 to 5 hours, depends on your system

## Data Sizes
- Total database size: **9.1 GB**, **59 551 925** rows, 20 tables
- Table `Actors`: **2 612 937** rows
- Table `Actresses`: **1 433 091** rows
- Table `Countries`: **252** rows
- Table `Directors`: **481 965** rows
- Table `Distributions`: **1 901 417** rows
- Table `DistributionsTags`: **3 871 667** rows
- Table `Distributors`: **101 840** rows
- Table `DistribTags`: **6 117** rows
- Table `DistribTagTypes`: **5** rows
- Table `Genres`: **34** rows
- Table `Movies`: **4 370 459** rows
- Table `MoviesActors`: **19 335 382** rows
- Table `MoviesActresses`: **11 827 956** rows
- Table `oviesCountries`: **2 015 581** rows
- Table `oviesDirectors`: **2 868 647** rows
- Table `MoviesGenres`: **2 534 912** rows
- Table `MovieTypes`: **4** rows
- Table `Plot`: **562 058** rows
- Table `Ratings`: **740 133** rows
- Table `Releases`: **4 887 468** rows
